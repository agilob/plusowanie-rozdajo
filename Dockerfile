FROM alpine:3.6
MAINTAINER agilob

WORKDIR /

# DNS stuff
RUN echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf

# SSL certs
RUN apk add --update ca-certificates \
    && rm -rf /var/cache/apk/*

# Binary
ADD plusowanieRozdajo /plusowanie-rozdajo

# Runtime
CMD ["/plusowanie-rozdajo"]
