#!/usr/bin/env bash
go get gitlab.com/agilob/go-wypok       # my own wypok api wrapper
go get github.com/jasonlvhit/gocron     # scheduler

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build