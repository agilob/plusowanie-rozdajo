package main

import (
	"github.com/jasonlvhit/gocron"

	"io/ioutil"
	"os"

	"gitlab.com/agilob/go-wypok"
	"./logger"
)

var wh *go_wypok.WykopHandler

func main() {
	initStructs()
	Logger.Init(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)

	Logger.Info.Println("Starting...")

	gocron.Every(1).Day().At("13:03").Do(wh.LoginToWypok)
	gocron.Every(1).Hour().Do(upvoteRozdajo)
	upvoteRozdajo()

	<-gocron.Start()
}

func initStructs() {

	appkey := os.Getenv("WYPOK_APPKEY")
	secret := os.Getenv("WYPOK_SECRET")
	connKey := os.Getenv("WYPOK_CONNECTION_KEY")

	wh = new(go_wypok.WykopHandler)
	wh.SetAppKey(appkey)
	wh.SetSecret(secret)
	wh.SetConnectionKey(connKey)

	wh.LoginToWypok()

	Logger.Init(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
}

func upvoteRozdajo() {
	entries, wypokError := wh.GetEntriesFromTag("rozdajo", 1)
	if wypokError != nil {
		Logger.Error.Println(wypokError.ErrorObject.Message)
	} else {
		upvoteEntries(wh, entries)
	}
}

func upvoteEntries(wh *go_wypok.WykopHandler, entries go_wypok.TagsEntries) {
	for i := range entries.Items {
		lastEntry := entries.Items[len(entries.Items)-1-i]
		Logger.Info.Printf("Checking entry %d", lastEntry.ID)
		if lastEntry.VoteCount >= 10 {
			Logger.Info.Printf("Upvoting entry %v with %d upvotes", lastEntry.ID, lastEntry.VoteCount)
			voteResponse, err := wh.UpvoteEntry(lastEntry.ID)
			if err != nil {
				Logger.Error.Println(err.ErrorObject.Message)
			} else {
				Logger.Trace.Printf("Upvoted %d", voteResponse.Vote)
			}
		} else {
			Logger.Info.Printf("Not enough upvotes")
		}
	}
}
